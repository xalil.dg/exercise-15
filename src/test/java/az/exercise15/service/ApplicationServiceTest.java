package az.exercise15.service;

import az.exercise15.entity.Calculation;
import az.exercise15.exception.InvalidOperationException;
import az.exercise15.repository.CalculationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ApplicationServiceTest {

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private CalculationRepository calculationRepository;

    @Test
    void testMultiply() {
        double result = applicationService.multiply(4, 5);
        assertEquals(20, result);
    }

    @Test
    void testSubtract() {
        double result = applicationService.subtract(10, 5);
        assertEquals(5, result);
        Calculation calculation = calculationRepository.findAll().get(1);
        assertEquals(10, calculation.getA());
        assertEquals(5, calculation.getB());
        assertEquals("subtract", calculation.getOperation());
        assertEquals(5, calculation.getResult());
    }

    @Test
    void testAddition() throws InvalidOperationException {
        double result = applicationService.addition(3, 5);
        assertEquals(8, result);
        Calculation calculation = calculationRepository.findAll().get(2);
        assertEquals(3, calculation.getA());
        assertEquals(5, calculation.getB());
        assertEquals("addition", calculation.getOperation());
        assertEquals(8, calculation.getResult());
    }

    @Test
    void testDivision() throws InvalidOperationException {
        double result = applicationService.division(4, 2);
        assertEquals(2, result);
        Calculation calculation = calculationRepository.findAll().get(3);
        assertEquals(4, calculation.getA());
        assertEquals(2, calculation.getB());
        assertEquals("division", calculation.getOperation());
        assertEquals(2, calculation.getResult());
    }

}