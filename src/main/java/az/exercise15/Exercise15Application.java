package az.exercise15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class Exercise15Application {

    public static void main(String[] args) {
        SpringApplication.run(Exercise15Application.class, args);
    }

}
