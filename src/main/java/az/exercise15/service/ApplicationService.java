package az.exercise15.service;

import az.exercise15.entity.Calculation;
import az.exercise15.exception.InvalidOperationException;
import az.exercise15.repository.CalculationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ApplicationService {

    private final CalculatorService calculatorService;

    private final CalculationRepository calculationRepository;

    public Calculation saveCalculation(double a, double b, String operation, double result) {
        Calculation calculation = new Calculation();
        calculation.setA(a);
        calculation.setB(b);
        calculation.setOperation(operation);
        calculation.setResult(result);
        return calculationRepository.save(calculation);
    }

    public double multiply(double a, double b) {
        double result = calculatorService.multiply(a, b);
        saveCalculation(a, b, "multiply", result);
        return result;
    }

    public double subtract(double a, double b) {
        double result = calculatorService.subtract(a, b);
        saveCalculation(a, b, "subtract", result);
        return result;
    }

    public double addition(double a, double b) throws InvalidOperationException {
        double result = calculatorService.addition(a, b);
        saveCalculation(a, b, "addition", result);
        return result;
    }

    public double division(double a, double b) throws InvalidOperationException {
        double result = calculatorService.division(a, b);
        saveCalculation(a, b, "division", result);
        return result;
    }
}
