package az.exercise15.service;

import az.exercise15.exception.InvalidOperationException;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    public double multiply(double a, double b) {
        return a * b;
    }

    public double subtract(double a, double b) {
        return a - b;
    }

    public double addition(double a, double b) throws InvalidOperationException {
        if (a < 0 || b < 0) {
            throw new InvalidOperationException("Only positive numbers are allowed for addition.");
        }
        return a + b;
    }

    public double division(double a, double b) throws InvalidOperationException {
        if (a % 2 != 0 || b % 2 != 0) {
            throw new InvalidOperationException("Only numbers divisible by 2 are allowed for division.");
        }
        if (b == 0) {
            throw new InvalidOperationException("Division by zero is not allowed.");
        }
        return a / b;
    }
}
